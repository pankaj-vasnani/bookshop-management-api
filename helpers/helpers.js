/**
 * Function to parse date in valid format
 * 
 * @param stringDate as String Date
 * 
 * @return Valid Date Format 
 */
parseDate = (stringDate) => {
    var d = new Date(stringDate),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) 
        month = '0' + month;
    if (day.length < 2) 
        day = '0' + day;

    return [year, month, day].join('-');
}


/**
 * Function to count number of words in the string
 * 
 * @param str as String
 * 
 * @return Number of words in the string
 */
countWordsInString = (str) => {
    return str.split(' ').length
}

/**
 * Function to check if object id exists in an array of Objectids
 * 
 * @param objectId as Schema Object Id
 * 
 * @return boolean value
 */
 checkObjectIdExistsInArray = (id, coll) => {
    var isInArray = coll.some(function (dt) {
        return dt.equals(id);
    })

    return isInArray
}

/**
 * Function to interpret json response
 * 
 * @param res as Response Object
 * @param statusType as Status Type String
 * @param statusCode as Status Code
 * @param data as Response Data
 * @param message as Response Message
 * 
 * @return Valid Response Format 
 */
interpretJsonResponse = (res, statusType, statusCode, data, message=null, errors=null) => {
    return res.status(statusCode).json({"success": statusType, "data": data, "message": message, "errors": errors});
}

module.exports.parseDate             = parseDate; 
module.exports.interpretJsonResponse = interpretJsonResponse; 
module.exports.countWordsInString = countWordsInString; 
module.exports.checkObjectIdExistsInArray = checkObjectIdExistsInArray; 