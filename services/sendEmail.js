const mailConfig = require('../config/mail')
const helpers = require('../helpers/helpers');
const nodemailer = require('nodemailer')

function sendMail(fromEmail, toEmail, subject, content) {
    // Create transporter
    var transporter = nodemailer.createTransport({
        service: mailConfig.email_service,
        auth: {
            user: mailConfig.email,
            pass: mailConfig.password
        }
    });

    var mailOptions = {
        from: fromEmail,
        to: toEmail,
        subject: subject,
        text: content
    };

      
    // Send email
    transporter.sendMail(mailOptions, function(error, info){
        if (error) {
            helpers.interpretJsonResponse(info, "false", 500, [], "Something went wrong.");
        }
      });
}

module.exports = sendMail
