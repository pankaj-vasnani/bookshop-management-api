const helpers = require('../helpers/helpers');

const UserService = require( "../services/Users/UserService" );
const UserServiceInstance = new UserService();
const userValidator = require("../validators/userFormRequest");
const CustomError = require("../errors/custom_error")
const logger = require('../config/winston')
const User = require('../models/User')
const bycrypt = require('bcryptjs')

/**
 * Update the user
 * @param req
 * @param res
 * 
 * @returns json response
 */
 async function updateProfile(req, res) {
    try{
        var validator = userValidator.validateUpdateRequest(req.body)
 
        validator.check().then(async function (matched) {
            if (matched) {
                let id = req.userId

                const salt = await bycrypt.genSalt(10);
                req.body.password = await bycrypt.hash(req.body.password, salt)

                let data = await UserServiceInstance.update(id, req.body)

                return helpers.interpretJsonResponse(res, "true", 200, [], "Profile successfully updated.");
            }
            
            throw new CustomError(422, null, validator.errors)
        }).catch( (error) => {
            logger.error(`message - ${error.message}, stack trace - ${error.stack}`);
            
            return helpers.interpretJsonResponse(res, "false", error.code, [], error.message, error.errors);
        });
    } catch (err) {
        logger.error(`message - ${err.message}, stack trace - ${err.stack}`);

        code = (err.code == undefined) ? 500 : err.code;

        return helpers.interpretJsonResponse(res, "false", code, [], err.message);
    }
 }


async function getPublishedBooks(req, res) {
    try{
        page = req.body.page
        search = req.body.search

        let data = await UserServiceInstance.getPublishedBooks(req.userId, page, search)
        
        return helpers.interpretJsonResponse(res, "true", 200, data, null);
    } catch (err) {
        logger.error(`message - ${err.message}, stack trace - ${err.stack}`);

        code = (err.code == undefined) ? 500 : err.code;

        return helpers.interpretJsonResponse(res, "false", code, [], err.message);
    }
}

async function getReadBooks(req, res) {
    try{
        page = req.body.page
        search = req.body.search

        let data = await UserServiceInstance.getReadBooks(req.userId, page, search)
        
        return helpers.interpretJsonResponse(res, "true", 200, data, null);
    } catch (err) {
        logger.error(`message - ${err.message}, stack trace - ${err.stack}`);

        code = (err.code == undefined) ? 500 : err.code;

        return helpers.interpretJsonResponse(res, "false", code, [], err.message);
    }
}

module.exports = {
    "updateProfile"   : updateProfile,
    "getPublishedBooks": getPublishedBooks,
    "getReadBooks": getReadBooks
};