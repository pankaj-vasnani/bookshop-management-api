const mongoose = require('mongoose');

// Connecting to database
connectToDatabase = (connectionString) => {
    mongoose.Promise = global.Promise;
    mongoose.connect(connectionString, { useNewUrlParser: true, useFindAndModify: false }).then(
    () =>
        {
            console.log('Database is connected') 
        },
    err => 
        { 
            console.log('Can not connect to the database'+ err)
        }
    );
}

module.exports.connectToDatabase = connectToDatabase;