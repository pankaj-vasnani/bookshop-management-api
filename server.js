require('dotenv').config()
const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const cors = require('cors');
const bookMgmtRoute = require('./routes/books.routes.js');
const authMgmtRoute = require('./routes/auth.routes.js');
const userMgmtRoute = require('./routes/users.routes.js');
const constants = require('./config/global_constants');
const db = require('./database/setup');
const verifyToken = require('./middleware/verifyToken')
const morgan = require('morgan')
const winston = require('./config/winston')

// Connecting to Database
let connectionString = process.env.DATABASE_CONNECTION_URL;
db.connectToDatabase(connectionString);

// Connecting to Node JS Server and using additional CORS and URL Encoding package and other related packages for the project
app.use(cors())
app.use(bodyParser.urlencoded({extended: true}))
app.use(bodyParser.json())

app.use(morgan('combined', { stream: winston.stream }));

// Defining the end point for node js routes to be accessible at the front end
const baseUrl = constants.baseUrl;
app.use(baseUrl+'/books', verifyToken, bookMgmtRoute);
// app.use(baseUrl+'/books', bookMgmtRoute);
app.use(baseUrl+'/users', verifyToken, userMgmtRoute);
app.use(baseUrl+'/auth', authMgmtRoute);

// Listening to port for any incoming connections from the client computers(front end)
const PORT = process.env.PORT;
module.exports = app.listen(PORT, function(){
  console.log('Server is running on Port:',PORT);
});