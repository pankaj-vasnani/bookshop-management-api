let v = require('node-input-validator');

validateUpdateRequest = (body) => {
    // Validate the form input data
    let validator = new v( body, 
        {
            name:'required',
            email: 'required|email',
            password:'nullable|min:6',
            mobile:'required'
        },
        {
            'name.required'         : 'Please enter your name.',
            'email.required'        : 'Please enter your email.',
            'email.email'           : 'Please enter your valid email address.',
            'password.min'          : 'Password must be 6 or more than 6 characters long.',
            'mobile.required' : 'Please enter your contact number.'
        }
    );
    
    return validator
}

validateRequest = (body) => {
    // Validate the form input data
    let validator = new v( body, 
        {
            name:'required',
            email: 'required|email',
            password:'required|min:6',
            mobile:'required'
        },
        {
            'name.required'         : 'Please enter your name.',
            'email.required'        : 'Please enter your email.',
            'email.email'           : 'Please enter your valid email address.',
            'password.required'          : 'Please enter your password.',
            'password.min'          : 'Password must be 6 or more than 6 characters long.',
            'mobile.required' : 'Please enter your contact number.'
        }
    );
    
    return validator
}

validateLoginRequest = (body) => {
    let validator = new v( body, 
        {
            email: 'required|email',
            password:'required|min:6'
        },
        {
            'email.required'        : 'Please enter your email.',
            'email.email'           : 'Please enter your valid email address.',
            'password.required'          : 'Please enter your password.',
            'password.min'          : 'Password must be 6 or more than 6 characters long.',
        }
    );
    
    return validator
}

validateVerifyFormRequest = (body) => {
    // Validate the form input data
    let validator = new v( body, 
        {
            email: 'required|email',
            code:'required'
        },
        {
            'email.required'        : 'Please enter your email.',
            'email.email'           : 'Please enter your valid email address.',
            'code.required'          : 'Please enter your code.',
        }
    );
    
    return validator
}

module.exports.validateRequest = validateRequest
module.exports.validateLoginRequest = validateLoginRequest
module.exports.validateVerifyFormRequest = validateVerifyFormRequest
module.exports.validateUpdateRequest = validateUpdateRequest
