const MongooseService = require( "../MongooseService" ); // Data Access Layer
const BookModel = require( "../../models/Book" ); // Database Model
const appConst = require("../../config/app");

class BookService {
  /**
   * @description Create an instance of PostService
   */
  constructor () {
    // Create instance of Data Access layer using our desired model
    this.MongooseServiceInstance = new MongooseService(BookModel);
  }

  /**
   * @description Attempt to create a post with the provided object
   * @param postToCreate {object} Object containing all required fields to
   * create post
   * @returns {Promise<{success: boolean, error: *}|{success: boolean, body: *}>}
   */
  async get (page, search) {
      const options = {
        page: page,
        limit: appConst.books_per_page_limit
      };
      
      var globalSearchObj = {}

      if(search != null || search != "") {
        var searchQuery = []

        searchQuery.push( { title:  { $regex: '.*' + search + '.*', $options: 'i' } }, { content: { $regex: '.*' + search + '.*', $options: 'i' } } )

        globalSearchObj.$or = searchQuery
      }

      const result = await BookModel.paginate(globalSearchObj, options);

      return result;
  }

  /**
   * @description Attempt to create a post with the provided object
   * @param postToCreate {object} Object containing all required fields to
   * create post
   * @returns {Promise<{success: boolean, error: *}|{success: boolean, body: *}>}
   */
  async getById(id) {
    const result = await this.MongooseServiceInstance.findById(id);

    return result;
  }

  /**
   * @description Attempt to create a post with the provided object
   * @param postToCreate {object} Object containing all required fields to
   * create post
   * @returns {Promise<{success: boolean, error: *}|{success: boolean, body: *}>}
   */
  async getByIdAndRemove(id) {
    const result = await this.MongooseServiceInstance.findByIdAndRemove(id);

    return result;
  }

  /**
   * @description Attempt to create a post with the provided object
   * @param postToCreate {object} Object containing all required fields to
   * create post
   * @returns {Promise<{success: boolean, error: *}|{success: boolean, body: *}>}
   */
  async create (body) {
    const result = await this.MongooseServiceInstance.create(body);
    
    return result;
  }
  /**
   * @description Attempt to create a post with the provided object
   * @param postToCreate {object} Object containing all required fields to
   * create post
   * @returns {Promise<{success: boolean, error: *}|{success: boolean, body: *}>}
   */
  async update (id, body) {
    const result = await this.MongooseServiceInstance.update(id, body);
    
    return result;
  }
}

module.exports = BookService;