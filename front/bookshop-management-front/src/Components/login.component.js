import '../App.css';
import {useState, useEffect} from 'react';

function Login() {
    const intialValues = { email: "", password: "" };
    const [formValues, setFormValues] = useState(intialValues);
    const [formErrors, setFormErrors] = useState({});
    const [isSubmitting, setIsSubmitting] = useState(false);

    const submitForm = () => {
        console.log(formValues);
      };
    
     const handleChange = (e) => {
        const { name, value } = e.target;
        setFormValues({ ...formValues, [name]: value });
      };
    
    const handleSubmit = (e) => {
        e.preventDefault();
        setFormErrors(validate(formValues));
        setIsSubmitting(true);
      };
    
    const closeModal = (e) => {
        setIsSubmitting(false);
    }

    const validate = (values) => {
        let errors = {};
        const regex = /^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/i;
        if (!values.email) {
          errors.email = "Please enter email.";
        } else if (!regex.test(values.email)) {
          errors.email = "Please enter valid email.";
        }
        if (!values.password) {
          errors.password = "Please enter password.";
        } else if (values.password.length < 4) {
          errors.password = "Password must be more than 4 characters";
        }
        return errors;
      };
    
    useEffect(() => {
        if (Object.keys(formErrors).length === 0 && isSubmitting) {
          submitForm();
        }
      }, [formErrors]);    

  return (
    <form onSubmit={handleSubmit} noValidate>
        {Object.keys(formErrors).length === 0 && isSubmitting && (        
            <div className="alert alert-success alert-dismissible fade show" role="alert">
                <strong>Success!</strong> Signed in successfully.
                <button onClick={closeModal} type="button" className="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        )}

        <h3>Sign In</h3>

        <div className="form-group">
            <label>Email address</label>
            <input type="email" name="email" id="email" value={formValues.email} onChange={handleChange} className={formErrors.email ? 'form-control input-error' : 'form-control'} placeholder="Enter email" />
            {formErrors.email && (
                <span className="error">{formErrors.email}</span>
          )}
        </div>

        <div className="form-group">
            <label>Password</label>
            <input type="password" name="password" id="password" onChange={handleChange} className={formErrors.password ? 'form-control input-error' : 'form-control'} placeholder="Enter password" />
            {formErrors.password && (
                <span className="error">{formErrors.password}</span>
            )}
        </div>

        <div className="form-group">
            <div className="custom-control custom-checkbox">
                <input type="checkbox" className="custom-control-input" id="customCheck1" />
                <label className="custom-control-label" htmlFor="customCheck1">Remember me</label>
            </div>
        </div>

        <button type="submit" className="btn btn-primary btn-block">Submit</button>
        <p className="forgot-password text-right">
            Forgot <a href="#">password?</a>
        </p>
    </form>
  );
}

export default Login;
