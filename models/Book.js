let mongoose = require("mongoose");
let mongoosePaginate = require('mongoose-paginate-v2');

// Creating the schema for user db
var bookSchema = new mongoose.Schema(
  {
    id : Number,
    title : String,
    isbn: String,
    content: String,
    numberOfPages: Number,
    publisher: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'User'
    },
    reader : [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref:'User'
      }
    ]
  },
  {
    timestamps: 
    {
        createdAt: 'created_at', 
        updatedAt: 'updated_at'
    }
  }
);

bookSchema.plugin(mongoosePaginate);

// Creating the model from the above schema
var Book = mongoose.model('Book', bookSchema);

// Exporting the model created above
module.exports = Book;