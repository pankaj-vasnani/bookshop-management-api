process.env.NODE_ENV = 'test';

let mocha = require('mocha');
let describe = mocha.describe;
let beforeEach = mocha.beforeEach;
let it = mocha.it;

let mongoose = require("mongoose");
let Book = require('../models/Book');

//Require the dev-dependencies
let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../server');
let should = chai.should();
const constants = require('../config/global_constants');


chai.use(chaiHttp);

let token = ''

//Our parent block
describe('Books', () => {
    beforeEach((done) => { 
        chai.request(server)
                .post(constants.baseUrl+'/auth/login')
                .send({email: 'pankaj_test15@mailinator.com', password: "123456"})
                .end((err, res) => {
                    token = res.body.data.token
                    done();
            });     
    });

    /*
    * Test the /GET route
    */
    describe('/GET books', () => {
        it('it should GET all the books', (done) => {
            chai.request(server)
                .get(constants.baseUrl+'/books')
                .set({ Authorization: `Bearer ${token}` })
                .end((err, res) => {
                    should.exist(res.body.data);
                    res.body.data.should.be.a('array');
                    done();
            });
        });
    });

    /*
    * Test the /POST route
    */
    describe('/POST books', () => {
        // Case 1 : Check if user does not enter the "title" field
        it('it should not POST a book without title field', (done) => {
            let book = {
                "isbn"           : "978-3-16-148410-0",
                "numberOfPages"  : 188,
                "content"        : "Hello, I am back again."
            }
        chai.request(server)
            .post(constants.baseUrl+'/books/')
            .set({ Authorization: `Bearer ${token}` })
            .send(book)
            .end((err, res) => {
                    should.exist(res.body);
                    res.should.have.status(422);
                    res.body.should.be.a('object');
                    res.body.should.have.property('errors');
                    res.body.errors.should.have.property('title');
                    res.body.errors.title.should.have.property('rule').eql('required');
                done();
            });
        });

        // Case 3 : Check if user does not enter the "isbn" field
        it('it should not POST a book without isbn field', (done) => {
            let book = {
                "title"          : "React JS Advanced",
                "numberOfPages"  : 188,
                "content"        : "Hello, I am back again."
            }
        chai.request(server)
            .post(constants.baseUrl+'/books/')
            .set({ Authorization: `Bearer ${token}` })
            .send(book)
            .end((err, res) => {
                    res.should.have.status(422);
                    res.body.should.be.a('object');
                    res.body.should.have.property('errors');
                    res.body.errors.should.have.property('isbn');
                    res.body.errors.isbn.should.have.property('rule').eql('required');
                done();
            });
        });

        // Case 4 : Check if user does not enter the "numberOfPages" field
        it('it should not POST a book without numberOfPages field', (done) => {
            let book = {
                "title"          : "React JS Advanced",
                "isbn"           : "978-3-16-148410-0",
                "content"        : "Hello, I am back again."
            }
        chai.request(server)
            .post(constants.baseUrl+'/books/')
            .set({ Authorization: `Bearer ${token}` })
            .send(book)
            .end((err, res) => {
                    res.should.have.status(422);
                    res.body.should.be.a('object');
                    res.body.should.have.property('errors');
                    res.body.errors.should.have.property('numberOfPages');
                    res.body.errors.numberOfPages.should.have.property('rule').eql('required');
                done();
            });
        });

        // Case 7 : Check if user does not enter the numerical value for "numberOfPages" field
        it('it should not POST a book with string type value for numberOfPages field', (done) => {
            let book = {
                "title"          : "React JS Advanced",
                "isbn"           : "978-3-16-148410-0",
                "numberOfPages"  : "adsdsdsd",
                "content"        : "Hello, I am back again."
            }
        chai.request(server)
            .post(constants.baseUrl+'/books/')
            .set({ Authorization: `Bearer ${token}` })
            .send(book)
            .end((err, res) => {
                    res.should.have.status(422);
                    res.body.should.be.a('object');
                    res.body.should.have.property('errors');
                    res.body.errors.should.have.property('numberOfPages');
                    res.body.errors.numberOfPages.should.have.property('rule').eql('numeric');
                done();
            });
        });
    });

    /*
     * Test the /PUT/:id route
     */
    describe('/PUT/:id books', () => {
        // Case 1 : Check if user entered all the details to update the book details
        it('it should UPDATE a book given the id', (done) => {
            let book = new Book({title: "The Chronicles of Narnia", isbn: "978-3-16-148410-0", numberOfPages: 778, content: "Hello, I am back again."})
            book.save((err, book) => {
                chai.request(server)
                .put(constants.baseUrl+'/books/' + book._id)
                .set({ Authorization: `Bearer ${token}` })
                .send({title: "The Chronicles of Narnia", isbn: "978-3-16-148410-0", numberOfPages: 778, content: "Hello, I am back again."})
                .end((err, res) => {
                    res.should.have.status(200);
                    done();
                });
            });
        });

        // Case 2 : Check if user entered all the details to update the book details without the book title
        it('it should not UPDATE a book given the id without the book title', (done) => {
            let book = new Book({title: "The Chronicles of Narnia", isbn: "978-3-16-148410-0", numberOfPages: 778, content: "Hello, I am back again."})
            book.save((err, book) => {
                chai.request(server)
                .put(constants.baseUrl+'/books/' + book._id)
                .set({ Authorization: `Bearer ${token}` })
                .send({isbn: "978-3-16-148410-0", numberOfPages: 778, content: "Hello, I am back again."})
                .end((err, res) => {
                        res.should.have.status(422);
                        res.body.should.be.a('object');
                        res.body.should.have.property('errors');
                        res.body.errors.should.have.property('title');
                        res.body.errors.title.should.have.property('rule').eql('required');
                    done();
                });
            });
        });

        // Case 4 : Check if user entered all the details to update the book details without the book isbn
        it('it should not UPDATE a book given the id without the book isbn', (done) => {
            let book = new Book({title: "The Chronicles of Narnia", isbn: "978-3-16-148410-0", numberOfPages: 778, content: "Hello, I am back again."})
            book.save((err, book) => {
                chai.request(server)
                .put(constants.baseUrl+'/books/' + book._id)
                .set({ Authorization: `Bearer ${token}` })
                .send({title: "The Chronicles of Narnia", numberOfPages: 778, content: "Hello, I am back again."})
                .end((err, res) => {
                        res.should.have.status(422);
                        res.body.should.be.a('object');
                        res.body.should.have.property('errors');
                        res.body.errors.should.have.property('isbn');
                        res.body.errors.isbn.should.have.property('rule').eql('required');
                    done();
                });
            });
        });

        // Case 6 : Check if user entered all the details to update the book details without the book's number of pages
        it('it should not UPDATE a book given the id without the book\'s number of pages', (done) => {
            let book = new Book({title: "The Chronicles of Narnia", isbn: "978-3-16-148410-0", numberOfPages: 778, content: "Hello, I am back again."})
            book.save((err, book) => {
                chai.request(server)
                .put(constants.baseUrl+'/books/' + book._id)
                .set({ Authorization: `Bearer ${token}` })
                .send({title: "The Chronicles of Narnia", isbn: "978-3-16-148410-0", content: "Hello, I am back again."})
                .end((err, res) => {
                        res.should.have.status(422);
                        res.body.should.be.a('object');
                        res.body.should.have.property('errors');
                        res.body.errors.should.have.property('numberOfPages');
                        res.body.errors.numberOfPages.should.have.property('rule').eql('required');
                    done();
                });
            });
        });

        // Case 8 : Check if user entered all the details to update the book details without entering the book's number of pages in numerical format
        it('it should not UPDATE a book given the id without entering the book\'s number of pages in numerical format', (done) => {
            let book = new Book({title: "The Chronicles of Narnia", isbn: "978-3-16-148410-0", numberOfPages: 778, content: "Hello, I am back again."})
            book.save((err, book) => {
                chai.request(server)
                .put(constants.baseUrl+'/books/' + book._id)
                .set({ Authorization: `Bearer ${token}` })
                .send({title: "The Chronicles of Narnia", isbn: "978-3-16-148410-0", numberOfPages: "354ggh", content: "Hello, I am back again."})
                .end((err, res) => {
                        res.should.have.status(422);
                        res.body.should.be.a('object');
                        res.body.should.have.property('errors');
                        res.body.errors.should.have.property('numberOfPages');
                        res.body.errors.numberOfPages.should.have.property('rule').eql('numeric');
                    done();
                });
            });
        });
    });

    /*
     * Test the /GET/:id route
     */
    describe('/GET/:id books', () => {
        it('it should GET a book by the given id', (done) => {
            let book = new Book({ title: "React JS Advanced", isbn: "978-3-16-148410-0", numberOfPages: 3500, content: "Hello, I am back again." });
            book.save((err, book) => {
                chai.request(server)
                .get(constants.baseUrl+'/books/' + book._id)
                .set({ Authorization: `Bearer ${token}` })
                .send(book)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.data.should.have.property('_id');
                    res.body.data.should.have.property('title');
                    res.body.data.should.have.property('isbn');
                    res.body.data.should.have.property('numberOfPages');
                    res.body.data.should.have.property('content');
                    done();
                });
            });
        });
    });

    /*
     * Test the /DELETE/:id route
     */
    describe('/DELETE/:id books', () => {
        it('it should DELETE a book given the id', (done) => {
            let book = new Book({title: "The Chronicles of Narnia", isbn: "242dsdsd", numberOfPages: 778, content: "Hello, I am back again."})
            book.save((err, book) => {
                chai.request(server)
                .delete(constants.baseUrl+'/books/' + book._id)
                .set({ Authorization: `Bearer ${token}` })
                .end((err, res) => {
                        res.should.have.status(204);
                    done();
                });
            });
        });
    });
});