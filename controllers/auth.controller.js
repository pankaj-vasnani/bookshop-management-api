let User = require('../models/User.js');
let v = require('node-input-validator');
const helpers = require('../helpers/helpers');
const bycrypt = require('bcryptjs')
const jwt = require("jsonwebtoken");
const constants = require('../config/global_constants');
const sendMail = require('../services/sendEmail');
const mailConfig = require('../config/mail')
const authConfig = require('../config/auth')


const UserService = require( "../services/Users/UserService" );
const UserServiceInstance = new UserService();
const userValidator = require("../validators/userFormRequest");

const CustomError = require("../errors/custom_error")
const logger = require('../config/winston')

/**
 * Register the user
 * @param req
 * @param res
 * 
 * @returns json response
 */
function register(req, res) {
    try{
        var validator = userValidator.validateRequest(req.body)
 
        validator.check().then(async function (matched) {
            if (matched) {
                let userAlreadyExistCount = await UserServiceInstance.findAlreadyRegisteredUser({email: req.body.email})

                if(userAlreadyExistCount == 0) {
                    const salt = await bycrypt.genSalt(10);
                    hashPassword = await bycrypt.hash(req.body.password, salt)

                    let pin = Date.now().toString();
                    pin = Math.ceil(Number(pin.substr(pin.length - 4)) + Math.random() * 100);

                    const userData = {
                        name: req.body.name,
                        email: req.body.email,
                        password: hashPassword,
                        mobile: req.body.mobile,
                        verification_code: pin
                    }

                    var result = await UserServiceInstance.create(userData)

                    // Send mail
                    emailContent = 'Dear '+userData.name+',<br/> Your account has been successfully created. Please verify the account by the code sent which is '+pin+'<br/> Thank you.'

                    // sendMail(mailConfig.from_address, userData.email, 'User Registration', emailContent);

                    return helpers.interpretJsonResponse(res, "true", 201, [], "Your account has been successfully created. Please verify the account with the code sent on your email.");
                } else {
                    throw new CustomError(400, "This email has already been taken. Please enter another email.")
                }
            }

            throw new CustomError(422, null, validator.errors)
        }).catch( (error) => {
            logger.error(`message - ${error.message}, stack trace - ${error.stack}`);

            return helpers.interpretJsonResponse(res, "false", error.code, [], error.message, error.errors);
        });
    } catch (err) {
        logger.error(`message - ${err.message}, stack trace - ${err.stack}`);

        code = (err.code == undefined) ? 500 : err.code;

        return helpers.interpretJsonResponse(res, "false", code, [], err.message);
    }
}

/**
 * Login the user
 * @param req
 * @param res
 * 
 * @returns json response
 */
function login(req, res) {
    try {
        // Validate the form input data
        var validator = userValidator.validateLoginRequest(req.body)
    
        validator.check().then(async function (matched) {
            if (matched) {
                var data = await UserServiceInstance.findUserByEmail(req.body.email)
                
                if(data != null) {
                    // Compare the password
                    // bycrypt.compare(req.body.password, data.password, function(err, result) {
                    bycrypt.compare(req.body.password, data.password).then( (result) => {
                        if(result == true) {
                            if(data.is_verified == constants.status.active) {
                                if(data.status == constants.status.active) {
                                    // Make the payload to generate the token
                                    const payload = {
                                        user: {
                                            id: data._id,
                                            email: data.email
                                        }
                                    }
                                    
                                    // Create the token and send it in response
                                    jwt.sign(payload, authConfig.secret, {},function(err, token) {
                                        if(err){
                                            throw new CustomError(500, "Something went wrong.")
                                        }

                                        let responseData = {
                                            token: token
                                        }

                                        return helpers.interpretJsonResponse(res, "true", 200, responseData, null);
                                    })
                                } else {
                                    throw new CustomError(400, "Your account has been deactivated by the administrator. Please contact admin to activate your account.")
                                }
                            } else {
                                throw new CustomError(400, "Your account is not verified. Please verify the account by the verification link sent in the mail.")
                            }
                        } else {
                            throw new CustomError(400, "The login credentials are invalid. Please enter again.")
                        }
                    }).catch( (err1) => {
                        logger.error(`message - ${err1.message}, stack trace - ${err1.stack}`);
            
                        return helpers.interpretJsonResponse(res, "false", err1.code, null, err1.message);
                    })
                } else {
                    throw new CustomError(400, "This email is not registered in our system.")
                }
            } 
            else {
                throw new CustomError(422, null, validator.errors)
            }
        }).catch( (error) => {
            logger.error(`message - ${error.message}, stack trace - ${error.stack}`);
            
            return helpers.interpretJsonResponse(res, "false", error.code, [], error.message, error.errors);
        })
    }  catch (err) {

        logger.error(`message - ${err.message}, stack trace - ${err.stack}`);

        code = (err.code == undefined) ? 500 : err.code;

        return helpers.interpretJsonResponse(res, "false", err.code, [], err.message, err.errors);
    }
}

/**
 * Verify the user account
 * @param req
 * @param res
 * 
 * @returns json response
 */
function verifyAccount(req, res) {
    try {
        // Validate the form input data
        var validator = userValidator.validateVerifyFormRequest(req.body)

        validator.check().then(async function (matched) {
            if (matched) {
                var data = await UserServiceInstance.findUserByEmail(req.body.email)

                if(data != null) {
                    let inputCode = req.body.code;
                    
                    if(inputCode == data.verification_code) {
                        await UserServiceInstance.update(data._id, {is_verified: constants.status.active, status: constants.status.active, verification_code: constants.status.inactive})

                        return helpers.interpretJsonResponse(res, "true", 200, [], "Your account has been successfully verified.");
                    } else {
                        throw new CustomError(400, "Please enter valid code.")
                    }
                } else {
                    throw new CustomError(400, "This email is not registered in our system.")
                }
            }

            throw new CustomError(422, null, validator.errors)
        }).catch( (error) => {
            logger.error(`message - ${error.message}, stack trace - ${error.stack}`);
            
            return helpers.interpretJsonResponse(res, "false", error.code, [], error.message, error.errors);
        })
    }  catch (err) {
        logger.error(`message - ${err.message}, stack trace - ${err.stack}`);

        code = (err.code == undefined) ? 500 : err.code;

        return helpers.interpretJsonResponse(res, "false", code, [], err.message);
    }
}

module.exports = {
    "register"   : register,
    "login"   : login,
    "verifyAccount": verifyAccount
};