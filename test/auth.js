process.env.NODE_ENV = 'test';

let mocha = require('mocha');
let describe = mocha.describe;
let beforeEach = mocha.beforeEach;
let it = mocha.it;

const User = require('../models/User')

//Require the dev-dependencies
let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../server');
let should = chai.should();
const constants = require('../config/global_constants');
let faker = require('faker');
const { resolve } = require('app-root-path');
var expect = chai.expect;


chai.use(chaiHttp); 


describe('Auth', () => {
    /*
    * Test the /POST route
    */
    describe('/POST auth/register', () => {
        // Case 1 : Check if user does not enter the "name" field
        it('it should not REGISTER the user without name field', (done) => {
            let user = {
                "name": "",
                "email": "pankaj_test15@mailinator.com",
                "password": "123456",
                "mobile": "+6282111807322"
            }

            chai.request(server)
            .post(constants.baseUrl+'/auth/register')
            .send(user)
            .end((err, res) => {
                    should.exist(res.body);
                    res.should.have.status(422);
                    res.body.should.be.a('object');
                    res.body.should.have.property('errors');
                    res.body.errors.should.have.property('name');
                    res.body.errors.name.should.have.property('rule').eql('required');
                done();
            });
        });

        // Case 2 : Check if user does not enter the "email" field
        it('it should not REGISTER the user without email field', (done) => {
            let user = {
                "name": "Pankaj 15",
                "email": "",
                "password": "123456",
                "mobile": "+6282111807322"
            }

            chai.request(server)
            .post(constants.baseUrl+'/auth/register')
            .send(user)
            .end((err, res) => {
                    should.exist(res.body);
                    res.should.have.status(422);
                    res.body.should.be.a('object');
                    res.body.should.have.property('errors');
                    res.body.errors.should.have.property('email');
                    res.body.errors.email.should.have.property('rule').eql('required');
                done();
            });
        });

        // Case 3 : Check if user does not enter the "password" field
        it('it should not REGISTER the user without password field', (done) => {
            let user = {
                "name": "Pankaj 15",
                "email": "pankaj_test15@mailinator.com",
                "password": "",
                "mobile": "+6282111807322"
            }

            chai.request(server)
            .post(constants.baseUrl+'/auth/register')
            .send(user)
            .end((err, res) => {
                    should.exist(res.body);
                    res.should.have.status(422);
                    res.body.should.be.a('object');
                    res.body.should.have.property('errors');
                    res.body.errors.should.have.property('password');
                    res.body.errors.password.should.have.property('rule').eql('required');
                done();
            });
        });

        // Case 4 : Check if user does not enter the "mobile" field
        it('it should not REGISTER the user without mobile field', (done) => {
            let user = {
                "name": "Pankaj 15",
                "email": "pankaj_test15@mailinator.com",
                "password": "123456",
                "mobile": ""
            }

            chai.request(server)
            .post(constants.baseUrl+'/auth/register')
            .send(user)
            .end((err, res) => {
                    should.exist(res.body);
                    res.should.have.status(422);
                    res.body.should.be.a('object');
                    res.body.should.have.property('errors');
                    res.body.errors.should.have.property('mobile');
                    res.body.errors.mobile.should.have.property('rule').eql('required');
                done();
            });
        });

        // Case 5 : Check if user does not enter with the duplicate "email" field
        it('it should not REGISTER the user with the duplicate email field', (done) => {
            let user = {
                "name": "Pankaj 15",
                "email": "pankaj_test15@mailinator.com",
                "password": "123456",
                "mobile": "+6282111807322"
            }

            chai.request(server)
            .post(constants.baseUrl+'/auth/register')
            .send(user)
            .end((err, res) => {
                    should.exist(res.body);
                    res.should.have.status(400);
                    res.body.should.be.a('object');
                done();
            });
        });

        // Case 6 : Check if user does not enter with the duplicate email
        it('it should not REGISTER the user with duplicate email field', (done) => {
            let user = {
                "name": "Pankaj 22",
                "email": "pankaj_test22@mailinator.com",
                "password": "123456",
                "mobile": "+6282111807322"
            }

            chai.request(server)
            .post(constants.baseUrl+'/auth/register')
            .send(user)
            .end((err, res) => {
                    should.exist(res.body);
                    res.should.have.status(400);
                    res.body.should.be.a('object');
                done();
            });
        });

        // Case 7 : Check if user enters all the fields
        it('it should REGISTER the user with all the fields', (done) => {
            let user = {
                "name": faker.name.firstName(),
                "email": faker.internet.email(),
                "password": faker.internet.password(),
                "mobile": faker.phone.phoneNumber()
            }

            chai.request(server)
            .post(constants.baseUrl+'/auth/register')
            .send(user)
            .end((err, res) => {
                    should.exist(res.body);
                    res.should.have.status(201);
                    res.body.should.be.a('object');
                done();
            });
        });
    })

    describe('/POST auth/verify-user-account', () => {
        // Case 1 : Check if user does not enter the "email" field
        it('it should not VERIFY the user without email field', (done) => {
            let user = {
                "email": "",
                "code": "8243"
            }

            chai.request(server)
            .post(constants.baseUrl+'/auth/verify-user-account')
            .send(user)
            .end((err, res) => {
                    should.exist(res.body);
                    res.should.have.status(422);
                    res.body.should.be.a('object');
                    res.body.should.have.property('errors');
                    res.body.errors.should.have.property('email');
                    res.body.errors.email.should.have.property('rule').eql('required');
                done();
            });
        });

        // Case 2 : Check if user does not enter the "code" field
        it('it should not VERIFY the user without code field', (done) => {
            let user = {
                "email": "pankaj_test22@mailinator.com",
                "code": ""
            }

            chai.request(server)
            .post(constants.baseUrl+'/auth/verify-user-account')
            .send(user)
            .end((err, res) => {
                    should.exist(res.body);
                    res.should.have.status(422);
                    res.body.should.be.a('object');
                    res.body.should.have.property('errors');
                    res.body.errors.should.have.property('code');
                    res.body.errors.code.should.have.property('rule').eql('required');
                done();
            });
        });

        // Case 3 : Check if user enters the invalid "code" field
        it('it should not VERIFY the user without the valid code field', (done) => {
            let user = {
                "email": "pankaj_test22@mailinator.com",
                "code": "8243"
            }

            chai.request(server)
            .post(constants.baseUrl+'/auth/verify-user-account')
            .send(user)
            .end((err, res) => {
                    should.exist(res.body);
                    res.should.have.status(400);
                    res.body.should.be.a('object');
                done();
            });
        });

        // Case 4 : Check if user enters all the fields
        it('it should VERIFY the user with the valid fields', async () => {
            let userData = {
                "name": faker.name.firstName(),
                "email": faker.internet.email(),
                "password": faker.internet.password(),
                "mobile": faker.phone.phoneNumber(),
                "verification_code": faker.datatype.number({
                    'min': 1000,
                    'max': 9999
                })
            }

            await User.create(userData)

            const result = await User.findOne().sort({ _id: -1 }).limit(1).exec()

            let user = {
                "email": result.email,
                "code": result.verification_code
            }

            chai.request(server)
            .post(constants.baseUrl+'/auth/verify-user-account')
            .send(user)
            .then(function(res){
                should.exist(res);
                expect(res).to.have.deep.property('status', 200);
            });
        });
    })

    describe('/POST auth/login', () => {
        // Case 1 : Check if user does not enter the "email" field
        it('it should not login the user without email field', (done) => {
            let user = {
                "email": "",
                "password": "123456"
            }

            chai.request(server)
            .post(constants.baseUrl+'/auth/login')
            .send(user)
            .end((err, res) => {
                should.exist(res.body);
                res.should.have.status(422);
                res.body.should.be.a('object');
                res.body.should.have.property('errors');
                res.body.errors.should.have.property('email');
                res.body.errors.email.should.have.property('rule').eql('required');
                done();
            });
        });

        // Case 2 : Check if user does not enter the valid "email" field
        it('it should not login the user without valid email field', (done) => {
            let user = {
                "email": "pankaj572",
                "password": "123456"
            }

            chai.request(server)
            .post(constants.baseUrl+'/auth/login')
            .send(user)
            .end((err, res) => {
                should.exist(res.body);
                res.should.have.status(422);
                res.body.should.be.a('object');
                res.body.should.have.property('errors');
                res.body.errors.should.have.property('email');
                res.body.errors.email.should.have.property('rule').eql('email');
                done();
            });
        });

        // Case 3 : Check if user does not enter the "password" field
        it('it should not login the user without password field', (done) => {
            let user = {
                "email": "pankaj572@gmail.com",
                "password": ""
            }

            chai.request(server)
            .post(constants.baseUrl+'/auth/login')
            .send(user)
            .end((err, res) => {
                should.exist(res.body);
                res.should.have.status(422);
                res.body.should.be.a('object');
                res.body.should.have.property('errors');
                res.body.errors.should.have.property('password');
                res.body.errors.password.should.have.property('rule').eql('required');
                done();
            });
        });

        // Case 4 : Check if user does not enter the regstered "email"
        it('it should not login the user without regstered email field', (done) => {
            let user = {
                "email": "pankaj572@gmail.com",
                "password": "1223234"
            }

            chai.request(server)
            .post(constants.baseUrl+'/auth/login')
            .send(user)
            .end((err, res) => {
                should.exist(res.body);
                res.should.have.status(400);
                res.body.should.be.a('object');
                res.body.should.have.property('message').eql('This email is not registered in our system.');
                done();
            });
        });

        // Case 5 : Check if user login the user with all the valid fields
        it('it should login the user with all the valid fields', (done) => {
            let user = {
                "email": "pankaj_test15@mailinator.com",
                "password": "123456777"
            }

            chai.request(server)
            .post(constants.baseUrl+'/auth/login')
            .send(user)
            .end((err, res) => {
                should.exist(res.body);
                res.should.have.status(400);
                res.body.should.be.a('object');
                res.body.should.have.property('message').eql('The login credentials are invalid. Please enter again.');
                done();
            });
        });

        // Case 6 : Check if user login the user with all the valid fields
        it('it should login the user with all the valid fields', (done) => {
            let user = {
                "email": "pankaj_test15@mailinator.com",
                "password": "123456"
            }

            chai.request(server)
            .post(constants.baseUrl+'/auth/login')
            .send(user)
            .end((err, res) => {
                should.exist(res.body);
                res.should.have.status(200);
                res.body.should.be.a('object');
                res.body.data.should.have.property('token');
                done();
            });
        });
    })
})