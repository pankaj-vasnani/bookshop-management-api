const MongooseService = require( "../MongooseService" ); // Data Access Layer
const UserModel = require( "../../models/User" ); // Database Model
const BookModel = require( "../../models/Book" );
const appConst = require("../../config/app");

class UserService {
    /**
     * @description Create an instance of PostService
     */
    constructor () {
      // Create instance of Data Access layer using our desired model
      this.MongooseServiceInstance = new MongooseService(UserModel);
      this.MongooseBookServiceInstance = new MongooseService(BookModel);
    }

    /**
     * @description Create a new document on the Model
     * @param body {object} Body object to create the new document with
     * @returns {Promise} Returns the results of the query
     */
    create ( body ) {
        return this.MongooseServiceInstance.create( body );
    }

    /**
     * @description Create a new document on the Model
     * @param body {object} Body object to create the new document with
     * @returns {Promise} Returns the results of the query
     */
    update ( id, body ) {
      return this.MongooseServiceInstance.update( id, body );
    }

    /**
     * @description Create a new document on the Model
     * @param body {object} Body object to create the new document with
     * @returns {Promise} Returns the results of the query
     */
    findAlreadyRegisteredUser ( query ) {
      return this.MongooseServiceInstance.countDocumentsInCollection( query );
    }
    /**
     * @description Create a new document on the Model
     * @param body {object} Body object to create the new document with
     * @returns {Promise} Returns the results of the query
     */
    findUserByEmail ( email ) {
      return this.MongooseServiceInstance.findOne( {email: email} );
    }

    /**
     * @description Create a new document on the Model
     * @param body {object} Body object to create the new document with
     * @returns {Promise} Returns the results of the query
     */
     findUserById ( id ) {
      return this.MongooseServiceInstance.findOne( {_id: id} );
    }

    /**
     * @description Create a new document on the Model
     * @param body {object} Body object to create the new document with
     * @returns {Promise} Returns the results of the query
     */
    async getPublishedBooks ( id, page, search ) {
      const options = {
        page: page,
        limit: appConst.books_per_page_limit
      };
      
      var globalSearchObj = {}

      if(search != null || search != "") {
        var searchQuery = []

        searchQuery.push( { title:  { $regex: '.*' + search + '.*', $options: 'i' } }, { content: { $regex: '.*' + search + '.*', $options: 'i' } } )

        globalSearchObj.$and = [ {publisher: id} ]
        globalSearchObj.$or = searchQuery
      }

      const result = this.MongooseBookServiceInstance.paginate(globalSearchObj, options)

      return result;
    }

    /**
     * @description Create a new document on the Model
     * @param body {object} Body object to create the new document with
     * @returns {Promise} Returns the results of the query
     */
    async getReadBooks ( id, page, search ) {
      const userIds =  [
          id
      ];

      const options = {
        page: page,
        limit: appConst.books_per_page_limit,
        populate: { path: 'publisher', select: '_id name' }
      };
      
      var globalSearchObj = {}

      if(search != null || search != "") {
        var searchQuery = []

        searchQuery.push( { title:  { $regex: '.*' + search + '.*', $options: 'i' } }, { content: { $regex: '.*' + search + '.*', $options: 'i' } } )

        globalSearchObj.$and = [ { 'reader': { $in: userIds } } ]
        globalSearchObj.$or = searchQuery
      }

      const result = this.MongooseBookServiceInstance.paginate(globalSearchObj, options)

      return result;
    }


}

module.exports = UserService;