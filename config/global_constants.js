const baseUrl = "/api";
const status = {
    active: 1,
    inactive: 0
}

module.exports = {
    "baseUrl": baseUrl,
    "status": status
}