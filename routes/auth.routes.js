let UserController = require('../controllers/auth.controller');

let express = require('express');
let userMgmtRoute = express.Router();

// Route for registering the user 
userMgmtRoute.route('/register').post(UserController.register);

// Route for login the user
userMgmtRoute.route('/login').post(UserController.login);

// Route for user verification
userMgmtRoute.route('/verify-user-account').post(UserController.verifyAccount);

module.exports = userMgmtRoute;


