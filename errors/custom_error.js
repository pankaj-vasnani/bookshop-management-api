class CustomError extends Error {
    constructor(code = 0, message = null, errors = null, ...params) {
      // Pass remaining arguments (including vendor specific ones) to parent constructor
      super(...params)
  
      // Maintains proper stack trace for where our error was thrown (only available on V8)
      if (Error.captureStackTrace) {
        Error.captureStackTrace(this, CustomError)
      }
  
      this.code = code
      this.message = message
      this.errors = errors
    }
}

module.exports = CustomError
  