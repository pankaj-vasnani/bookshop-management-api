let v = require('node-input-validator');

validateRequest = (body) => {

    // Validate the form input data
    let validator = new v( body, 
        {
            title:'required',
            isbn:'required',
            numberOfPages:'required|numeric',
            content: 'required'
        },
        {
            'title.required'         : 'Please enter book title.',
            'content.required'       : 'Please enter book content.',
            'isbn.required'          : 'Please enter book isbn.',
            'numberOfPages.required' : 'Please enter number of pages of the book.',
            'numberOfPages.numeric'  : 'Please enter the number of pages in numerical format.'
        }
    );
    
    return validator
}

module.exports.validateRequest = validateRequest