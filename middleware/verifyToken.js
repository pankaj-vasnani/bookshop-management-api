var jwt = require('jsonwebtoken');
const authConfig = require('../config/auth')
const helpers = require('../helpers/helpers');

function verifyToken(req, res, next) {
  var token = req.headers['authorization'].replace("Bearer ", "");

  if (token == null || token == undefined) {
    helpers.interpretJsonResponse(res, "false", 403, [], "Failed to authenticate the user.", null);
  } else {
    jwt.verify(token, authConfig.secret, function(err, decoded) {
        if (err)
            helpers.interpretJsonResponse(res, "false", 500, [], "Failed to authenticate the user.", null);
            // if everything good, save to request for use in other routes
            req.userId = decoded.user.id
            next();
    });
  }
}

module.exports = verifyToken;