const helpers = require('../helpers/helpers');

const BookService = require( "../services/Books/BookService" );
const BookServiceInstance = new BookService();
const bookValidator = require("../validators/bookFormRequest");
const CustomError = require("../errors/custom_error")
const logger = require('../config/winston')

const UserService = require( "../services/Users/UserService" );
const UserServiceInstance = new UserService();

/**
 * Get all the books
 * @param req
 * @param res
 * @returns void
 */
async function getBooks(req, res) {
    try{
        var page = req.body.page
        var search = req.body.search
        
        // Query the DB and if no errors, send all the books
        let result = await BookServiceInstance.get(page, search);

        return helpers.interpretJsonResponse(res, "true", 200, result); 
    } catch ( err ) {
        logger.error(`message - ${err.message}, stack trace - ${err.stack}`);

        return helpers.interpretJsonResponse(res, "false", 500, [],  'Internal Server Error');
    }
}

/**
 * Search the book
 * @param req
 * @param res
 * @returns void
 */
async function searchBook(req, res) {
    try{
        let id = req.params.id;

        let book = await BookServiceInstance.getById(id)

        if(book == null) {
            throw new CustomError(404, "Data Not Found", null)
        }

        return helpers.interpretJsonResponse(res, "true", 200, book, null);
    } catch (err) {
        logger.error(`message - ${err.message}, stack trace - ${err.stack}`);

        code = (err.code == undefined) ? 500 : err.code;

        return helpers.interpretJsonResponse(res, "false", code, [], err.message);
    }
}

/**
 * Store the book
 * @param req
 * @param res
 * @returns void
 */
function saveBook(req, res) {
    try{
        var validator = bookValidator.validateRequest(req.body)

        validator.check().then(async function (matched) {
            if (matched) {     
                req.body.publisher = req.userId
                req.body.reader = []
                
                let result = await BookServiceInstance.create(req.body)

                let user = await UserServiceInstance.findUserById(req.userId)
                
                user.books_published.push(result._id)
                user.total_words_written += helpers.countWordsInString(result.content)
    
                await UserServiceInstance.update(user._id, user)

                return helpers.interpretJsonResponse(res, "true", 201, result, "Book successfully saved");
            }

            throw new CustomError(422, null, validator.errors)
        }).catch( (error) => {
            logger.error(`message - ${error.message}, stack trace - ${error.stack}`);

            return helpers.interpretJsonResponse(res, "false", error.code, [], error.message, error.errors);
        });
    } catch (err) {
        logger.error(`message - ${err.message}, stack trace - ${err.stack}`);

        code = (err.code == undefined) ? 500 : err.code;

        return helpers.interpretJsonResponse(res, "false", code, [], err.message, err.errors);
    }
}


/**
 * Read the book
 * @param req
 * @param res
 * @returns void
 */
async function readBook(req, res) {
    try{
        let user = await UserServiceInstance.findUserById(req.userId)

        var isInArray = helpers.checkObjectIdExistsInArray(req.body.id, user.books_published)

        if(isInArray == false) {

            let result = await BookServiceInstance.getById(req.body.id)

            var isBookReadAlreadyExists = helpers.checkObjectIdExistsInArray(req.body.id, user.books_read)

            if(isBookReadAlreadyExists == false) {
                user.books_read.push(req.body.id)
            }

            user.total_words_read += helpers.countWordsInString(result.content)

            await UserServiceInstance.update(user._id, user)

            // Update reader array of books model
            let bookReadData = await BookServiceInstance.getById(req.body.id)

            bookReadData.reader.push(req.userId)

            await BookServiceInstance.update(req.body.id, bookReadData)

            return helpers.interpretJsonResponse(res, "true", 200, [], "Book successfully read");
        } else {
            return helpers.interpretJsonResponse(res, "true", 422, [], "Book published by the logged in author");
        }
    } catch (err) {
        logger.error(`message - ${err.message}, stack trace - ${err.stack}`);

        code = (err.code == undefined) ? 500 : err.code;

        return helpers.interpretJsonResponse(res, "false", code, [], err.message, err.errors);
    }
}

/**
 * Update the book
 * @param req
 * @param res
 * @returns void
 */
async function updateBook(req, res) {
    try {
        let id = req.params.id

        let book = await BookServiceInstance.getById(id)
        
        if(book == null) {
            throw new CustomError(404, "Data Not Found");
        }

        var validator = bookValidator.validateRequest(req.body)

        validator.check().then(async function (matched) {
            if(matched) {
                let result = await BookServiceInstance.update(id, req.body)

                return helpers.interpretJsonResponse(res, "true", 200, result, "Book successfully updated");
            }

            throw new CustomError(422, null, validator.errors)
        }).catch( (error) => {
            return helpers.interpretJsonResponse(res, "false", error.code, [], error.message, error.errors);
        });
    } catch (err) {
        logger.error(`message - ${err.message}, stack trace - ${err.stack}`);

        code = (err.code == undefined) ? 500 : err.code;

        return helpers.interpretJsonResponse(res, "false", code, [], err.message, err.errors);
    }
}

/**
 * Delete the book
 * @param req
 * @param res
 * @returns void
 */
async function deleteBook(req, res) {
    try {
        let id = req.params.id

        const result = await BookServiceInstance.getById(id)

        if(result == null || result == undefined) {
            throw new CustomError(404, "Data Not Found");
        }

        const data = await BookServiceInstance.getByIdAndRemove(id)

        if(data != null) {
            return helpers.interpretJsonResponse(res, "true", 204, [], "Book Successfully Deleted.");
        }
            
        throw new CustomError(500, "Something went wrong.");
        
    } catch ( err ) {
        logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
        
        code = (err.code == undefined) ? 500 : err.code;

        return helpers.interpretJsonResponse(res, "false", code, [], err.message);
    }
}

module.exports = {
    "getBooks"   : getBooks,
    "saveBook"   : saveBook,
    "updateBook" : updateBook,
    "deleteBook" : deleteBook,
    "searchBook" : searchBook,
    "readBook"   : readBook
};