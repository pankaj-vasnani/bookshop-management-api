require('dotenv').config()

const config = {
    secret: process.env.JWT_STRING,
    expiration_time: process.env.JWT_EXPIRATION_TIME
}

module.exports = config