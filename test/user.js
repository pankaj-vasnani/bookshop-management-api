process.env.NODE_ENV = 'test';

let mocha = require('mocha');
let describe = mocha.describe;
let beforeEach = mocha.beforeEach;
let it = mocha.it;

const User = require('../models/User')

//Require the dev-dependencies
let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../server');
let should = chai.should();
const constants = require('../config/global_constants');
let faker = require('faker');
const { resolve } = require('app-root-path');
var expect = chai.expect;

let token = ''

//Our parent block
describe('User', () => {
    beforeEach((done) => { 
        chai.request(server)
                .post(constants.baseUrl+'/auth/login')
                .send({email: 'pankaj_test15@mailinator.com', password: "123456"})
                .end((err, res) => {
                    token = res.body.data.token
                    done();
            });     
    });

    /*
    * Test the /POST route
    */
    describe('/POST update profile', () => {
        // Case 1 : Check if user update the profile without "name" field
        it('it should not update the profile without name field', (done) => {
            let user = {
                "name": "",
                "email": "pankaj_test120@mailinator.com",
                "password": "123456",
                "mobile": "+6282111807322"
            }
        chai.request(server)
            .post(constants.baseUrl+'/users/update-profile/')
            .set({ Authorization: `Bearer ${token}` })
            .send(user)
            .end((err, res) => {
                    should.exist(res.body);
                    res.should.have.status(422);
                    res.body.should.be.a('object');
                    res.body.should.have.property('errors');
                    res.body.errors.should.have.property('name');
                    res.body.errors.name.should.have.property('rule').eql('required');
                done();
            });
        });

        // Case 2 : Check if user update the profile without "email" field
        it('it should not update the profile without email field', (done) => {
            let user = {
                "name": "jack",
                "email": "",
                "password": "123456",
                "mobile": "+6282111807322"
            }
        chai.request(server)
            .post(constants.baseUrl+'/users/update-profile/')
            .set({ Authorization: `Bearer ${token}` })
            .send(user)
            .end((err, res) => {
                    should.exist(res.body);
                    res.should.have.status(422);
                    res.body.should.be.a('object');
                    res.body.should.have.property('errors');
                    res.body.errors.should.have.property('email');
                    res.body.errors.email.should.have.property('rule').eql('required');
                done();
            });
        });

        // Case 3 : Check if user update the profile without valid "email" field
        it('it should not update the profile without valid email field', (done) => {
            let user = {
                "name": "jack",
                "email": "djkhsdhs",
                "password": "123456",
                "mobile": "+6282111807322"
            }
        chai.request(server)
            .post(constants.baseUrl+'/users/update-profile/')
            .set({ Authorization: `Bearer ${token}` })
            .send(user)
            .end((err, res) => {
                    should.exist(res.body);
                    res.should.have.status(422);
                    res.body.should.be.a('object');
                    res.body.should.have.property('errors');
                    res.body.errors.should.have.property('email');
                    res.body.errors.email.should.have.property('rule').eql('email');
                done();
            });
        });

        // Case 4 : Check if user update the profile without "mobile number" field
        it('it should not update the profile without mobile number field', (done) => {
            let user = {
                "name": "jack",
                "email": "jack@mailinator.com",
                "password": "123456",
                "mobile": ""
            }
        chai.request(server)
            .post(constants.baseUrl+'/users/update-profile/')
            .set({ Authorization: `Bearer ${token}` })
            .send(user)
            .end((err, res) => {
                    should.exist(res.body);
                    res.should.have.status(422);
                    res.body.should.be.a('object');
                    res.body.should.have.property('errors');
                    res.body.errors.should.have.property('mobile');
                    res.body.errors.mobile.should.have.property('rule').eql('required');
                done();
            });
        });

        // Case 5 : Check if user update the profile with all the valid fields
        it('it should update the profile with all the valid fields', (done) => {
            let user = {
                "name": "jack",
                "email": "pankaj_test15@mailinator.com",
                "password": "123456",
                "mobile": "+6282111806522"
            }
        chai.request(server)
            .post(constants.baseUrl+'/users/update-profile/')
            .set({ Authorization: `Bearer ${token}` })
            .send(user)
            .end((err, res) => {
                    should.exist(res.body);
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                done();
            });
        });
    });

    describe('/POST read book', () => {
        // Case 1 : Check if user can read the book published by the user itself
        it('it should read the book published by the user itself', (done) => {
        let data = {
            "id": "60b7b26cee19cc4210a8395e"
        }

        chai.request(server)
            .post(constants.baseUrl+'/books/read-book/')
            .set({ Authorization: `Bearer ${token}` })
            .send(data)
            .end((err, res) => {
                    should.exist(res.body);
                    res.should.have.status(500);
                    res.body.should.be.a('object');
                done();
            });
        });

        // Case 2 : Check if user can read the book published by the another user
        it('it should read the book published by the another user', (done) => {
            let data = {
                "id": "60ba5df652752f1228b6155b"
            }
    
            chai.request(server)
                .post(constants.baseUrl+'/books/read-book/')
                .set({ Authorization: `Bearer ${token}` })
                .send(data)
                .end((err, res) => {
                        should.exist(res.body);
                        res.should.have.status(200);
                        res.body.should.be.a('object');
                        res.body.should.have.property('message').eql('Book successfully read');
                    done();
                });
        });

        // Case 3 : Check if user can read the book not published yet
        it('it should read the book not published yet', (done) => {
            let data = {
                "id": "61ba5df652752f1228b6155b"
            }
    
            chai.request(server)
                .post(constants.baseUrl+'/books/read-book/')
                .set({ Authorization: `Bearer ${token}` })
                .send(data)
                .end((err, res) => {
                        should.exist(res.body);
                        res.should.have.status(500);
                        res.body.should.be.a('object');
                        res.body.should.have.property('message').eql("Cannot read property 'content' of null");
                    done();
                });
        });

        // Case 4 : Check if user can read the book with invalid mongo schema id
        it('it should read the book with invalid mongo schema id', (done) => {
            let data = {
                "id": "61ba5df652752f1228b6155bd"
            }
    
            chai.request(server)
                .post(constants.baseUrl+'/books/read-book/')
                .set({ Authorization: `Bearer ${token}` })
                .send(data)
                .end((err, res) => {
                        should.exist(res.body);
                        res.should.have.status(500);
                        res.body.should.be.a('object');
                        res.body.should.have.property('message').eql("Cast to ObjectId failed for value \"61ba5df652752f1228b6155bd\" at path \"_id\" for model \"Book\"");
                    done();
                });
        });
    });

    /*
    * Test the /GET route
    */
    describe('/GET access published books', () => {
        // Case 1 : Check if user can access the published books
        it('it should not access the logged in user published books', (done) => {

        chai.request(server)
            .get(constants.baseUrl+'/users/published-books/')
            .set({ Authorization: `Bearer ${token}` })
            .end((err, res) => {
                    should.exist(res.body);
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.data.should.have.property('books_published');
                done();
            });
        });
    });

    describe('/GET access read books', () => {
        // Case 1 : Check if user can access the read books
        it('it should not access the logged in user read books', (done) => {

        chai.request(server)
            .get(constants.baseUrl+'/users/read-books/')
            .set({ Authorization: `Bearer ${token}` })
            .end((err, res) => {
                    should.exist(res.body);
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.data.should.have.property('books_read');
                done();
            });
        });
    });
})