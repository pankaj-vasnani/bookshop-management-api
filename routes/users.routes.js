// const multer = require('multer');
// const upload = multer({dest: __dirname + '/uploads/images'})
let UserController = require('../controllers/user.controller');
let express = require('express');
let userMgmtRoute = express.Router();

// Route for user update profile
userMgmtRoute.route('/update-profile').post(UserController.updateProfile);

// Route for fetching user's published books
userMgmtRoute.route('/published-books').get(UserController.getPublishedBooks);

// Route for fetching user's read books
userMgmtRoute.route('/read-books').get(UserController.getReadBooks);

module.exports = userMgmtRoute;


