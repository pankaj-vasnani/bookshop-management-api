let mongoose = require("mongoose");
let mongoosePaginate = require('mongoose-paginate-v2');

// Creating the schema for user db
var userSchema = new mongoose.Schema(
  {
    name : String,
    email : String,
    password: String,
    mobile: String,
    status: {type: Number, default: 0},
    is_verified: {type: Number, default: 0},
    total_words_read: {type: Number, default: 0},
    total_words_written: {type: Number, default: 0},
    verification_code: Number,
    books_published : [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref:'Book'
      }
    ],
    books_read : [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref:'Book'
      }
    ]
  },
  {
    timestamps: 
    {
        createdAt: 'created_at', 
        updatedAt: 'updated_at'
    }
  }
);

userSchema.plugin(mongoosePaginate);

// Creating the model from the above schema
var User = mongoose.model('User', userSchema);

// Exporting the model created above
module.exports = User;